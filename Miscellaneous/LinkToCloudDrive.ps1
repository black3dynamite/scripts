﻿<#

# New cloud drive user setup
.\LinkToCloudDrive.ps1 -UserName john.doe -CloudDriveFolder -NewCloudUser

# Previous cloud drive user setup
.\LinkToCloudDrive.ps1 -UserName john.doe -CloudDriveFolder

#>

[CmdletBinding()]
param (
  [Parameter(Mandatory=$True)]
  [string] $UserName = $(
    if ($UserName) {
      Read-Host
    }
  ),
  [Parameter(Mandatory=$True)]
  [string] $CloudDriveFolder = $(
    if ($CloudDriveFolder) {
      Read-Host
    }
  ),
  [Parameter(Mandatory=$False)]
  [switch] $NewCloudUser
)

$UserProfile = "C:\Users\$UserName"
$UserCloudDrive = "$UserProfile\$CloudDriveFolder"
$SourceFolders = "$UserProfile\Desktop", "$UserProfile\Documents", "$UserProfile\Music", "$UserProfile\Pictures", "$UserProfile\Videos"

Function NewCloudDriveUser {
  foreach ($Folder in $SourceFolders){
    $Destination = $Folder -creplace "(?s)^.*", "$UserCloudDrive"
    Move-Item $Folder $Destination -Force
  }
}

#Function for creating symbolic links
#Loop of 5 tries (10 seconds total) to rename folders
Function SymLink($Folder) {
  $Stoploop = $false
  [int]$Retrycount = "0"
  $FolderOld = $Folder + "_Old"

  do {
    try {
        if (Test-Path $UserProfile\$Folder) {
          $file = Get-Item $UserProfile\$Folder -Force -ErrorAction SilentlyContinue
          #if the $Folder is NOT a Symbolic Link
          if ([bool]($file.Attributes -band [IO.FileAttributes]::ReparsePoint) -ne "True") {
              #Rename it to _Old and set it as a hidden system folder (in case of file move issues)
              Rename-Item "$UserProfile\$Folder" "$UserProfile\$FolderOld" -Force
              $(Get-Item $UserProfile\$FolderOld).Attributes = "Hidden","System"
          } ELSE {
              #Else Use RMDIR to remove the Symbolic Link - Prevents removal of data from the redirected folder
              Start-Process -file "cmd.exe" -arg "/c rmdir `"$UserProfile\$Folder`"" -Wait -WindowStyle Hidden
          }
        }
        #Create the Symbolic Link
        #Start-Process -file "cmd.exe" -arg "/c mklink /J `"$UserProfile\$Folder`" `"$UserCloudDrive\$Folder`"" -Wait -NoNewWindow
        New-Item -ItemType Junction -Path "$UserProfile\$Folder" -Value "$UserCloudDrive\$Folder"
        $Stoploop = $true
        Return $Null
    }
    catch {
      #If the retries reach 5, exit the loop
      if ($Retrycount -gt 5){
        $Stoploop = $true
        Return $Null
      }
      else {
        #Sleep for 1 second then loop again
        Start-Sleep -Seconds 1
        $Retrycount = $Retrycount + 1
      }
    }
  }
  While ($Stoploop -eq $false)
  Return $Null
}

Function SymLinkFolders {

  #Perform the functions and sleep in between
  Symlink "Desktop"
  Start-Sleep 2
  Symlink "Documents"
  Start-Sleep 2
  Symlink "Music"
  Start-Sleep 2
  Symlink "Pictures"
  Start-Sleep 2
  Symlink "Videos"

}

Function ConfirmJunctionLinks {

  Get-Childitem -Path $UserProfile | Where-Object {$_.LinkType} | Select-Object FullName,LinkType,Target

}

if ($NewCloudUser.IsPresent) {

  NewCloudDriveUser
  SymLinkFolders
  ConfirmJunctionLinks

}
else {

  SymLinkFolders
  ConfirmJunctionLinks
    
}
