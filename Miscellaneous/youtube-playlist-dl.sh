#!/usr/bin/env bash

<<COMMENT

COMMENT

# set -o nounset
# set -o errexit
# set -o xtrace
# set -o pipefail
# set -o verbose
# set -o noexec

# shopt -s extglob

for YOUTUBE_PLAYLIST_DL in "$@"
do
/usr/bin/youtube-dl --output '%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' $YOUTUBE_PLAYLIST_DL
done