
[CmdletBinding()]
param (
    [Parameter()]
    [string]$YouTubePlaylistDL
)

Clear-Host

$_oOutputTemplates = @(
'--output "%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s"'
$YouTubePlaylistDL
)

Start-Process -FilePath 'youtube-dl.exe' -ArgumentList $_oOutputTemplates