#!/usr/bin/env bash

# set -o nounset
# set -o errexit
# set -o xtrace
# set -o pipefail
# set -o verbose
# set -o noexec

APP_NAME='hugo'
APP_PATH="${HOME}/Applications/${APP_NAME}"
APP_BIN="${HOME}/bin"
APP_FILENAME='hugo.tar.gz'
API_URL='https://api.github.com/repos/gohugoio/hugo/releases/latest'
OUTPUT_DIR='/tmp'

clear

function hugo_crossplatform () {
 
  install_dependencies
  echo -e '\n'
  
  create_directories
  echo -e '\n'

  install_hugo
  echo -e '\n'

  hugo_installed
  echo -e '\n'

}

function install_dependencies () {

  echo -e '\n'
  # Is jq installed? If not, install it automatically
  if hash jq 2>/dev/null; then
    echo "jq is already installed"
  else
    echo "jq not installed, installing"
    sudo dnf install --assumeyes jq
  fi

  # Is curl installed? If not, install it automatically
  if hash curl 2>/dev/null; then
    echo "curl is already installed"
  else
    echo "curl not installed, installing"
    sudo dnf install --assumeyes curl
  fi

  # Is tar installed? If not, install it automatically
  if hash tar 2>/dev/null; then
    echo "tar is already installed"
  else
    echo "tar not installed, installing"
    sudo dnf install --assumeyes tar
  fi

  # Is wget installed? If not, install it automatically
  if hash wget 2>/dev/null; then
    echo "wget is already installed"
  else
    echo "wget not installed, installing"
    sudo dnf install --assumeyes wget
  fi

}

function create_directories () {

  # If not exist, create Hugo directory 
  if [ -d ${APP_PATH} ] ; then
    echo "${APP_PATH} found"
  else
    echo "${APP_PATH} not found, creating it now."
    mkdir -p "${APP_PATH}"
  fi

  echo -e '\n'

  # If not exist, create bin directory
  if [ -d ${APP_BIN} ] ; then
    echo "${APP_BIN} found"
  else
    echo "${APP_BIN} not found, creating it now."
    mkdir -p "${APP_BIN}"
  fi

}

function install_hugo () {
clear
  cd "${OUTPUT_DIR}"

  HUGO_TYPE=Linux-64bit.tar.gz
  HUGO_LATEST=$(curl --silent ${API_URL} | jq --raw-output ".assets[] | select(.name | test(\"${HUGO_TYPE}\")) | .browser_download_url")
  HUGO_FILE=$(echo ${HUGO_LATEST} | awk '{print $2}')
  HUGO_LATEST_VERSION=$(curl --silent ${API_URL} | grep 'tag_name' | awk '{print $2}' | sed 's/"//g; s/,//g')
  HUGO_INSTALLED_VERSION=$(hugo version | awk '{print $5}' | sed 's/-[^-]*$//g')

  if [ ${HUGO_INSTALLED_VERSION} != ${HUGO_LATEST_VERSION} ]; then
    echo 'Older version detected, upgrading now.'
    echo ''
    wget --output-document=/tmp/${APP_FILENAME} "${HUGO_FILE}" && gzip -dc ${APP_FILENAME} | tar -xf - -C "${APP_PATH}"
    
    # Create symbolic link to hugo executable
    ln -sf "${APP_PATH}/${APP_NAME}" "${APP_BIN}/${APP_NAME}"
  else
    echo 'Latest version already installed!'
    echo ''
    exit 0
  fi

}

function hugo_installed () {

  # Confirm Hugo is installed
  echo 'Confirm Hugo is installed'
  if hash hugo 2>/dev/null; then
    hugo version
    rm "${OUTPUT_DIR}/${APP_FILENAME}"
  fi

}

function hugo_uninstall () {

  echo -e '\n'
  echo "Removing ${APP_PATH}"
  echo "Removing ${APP_BIN}/${APP_NAME}"
  rm --recursive ${APP_PATH} 2>/dev/null
  rm --recursive "${APP_BIN}/${APP_NAME}" 2>/dev/null
  echo -e '\n'

}

trap '' 2
while true
do
    clear
    echo '========================'
    echo "Install Hugo"
    echo '========================'
    echo 'Enter 1 to install Hugo: '
    echo 'Enter 0 to uninstall Hugo: '   
    echo 'Enter q to exit the menu: '
    echo -e '\n'
    echo -e 'Enter your selection: \c'
    read answer
    case "${answer}" in
      1)
        hugo_crossplatform
        break
        ;;
      0)
        hugo_uninstall
        break
        ;;
      q)
        echo 'Goodbye'
        exit
        ;;
      *)
        echo 'Invalid Option'
        ;;
    esac
    echo -e 'Press Enter to continue \c'
    read input
done

<<COMMENT
# tar commands tips
# gzip -dc archive.tar.gz | tar -xf - -C /destination
# tar xzf archive.tar.gz -C /destination
COMMENT